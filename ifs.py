#Se importa libreria y herramientas de la misma
from PIL import  Image, ImageDraw, ImageFont
#Se abre la imagen y se convierte a rgba para manipulacion
base = Image.open('prueba.jpg').convert('RGBA')
#se crea una base o capa adicional donde se introducira el texto
txt = Image.new('RGBA',base.size, (255,255,255,0))
#se declara tipo de letra que se va a usar
fnt= ImageFont.truetype('arial.ttf', 40)
d=ImageDraw.Draw(txt)
#Se realiza proceso de escritura en la imagen posicion no definida
texto = input("Ingrese lo que va a escribir: ")

d.text((1200, 700), texto, font=fnt, fill=(255,255,255,128))
#se hace la union de la capa con texto y la imagen 
out=Image.alpha_composite(base, txt)
#se muestra y guarda la imagen procesada 
out.show()
#out.save('nueva.jpg')